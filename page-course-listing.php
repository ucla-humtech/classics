<?php
/*
 Template Name: Course Listing
*/
?>
<?php get_header(); ?>
	<div class="content main">
		<div class="col" id="main-content" role="main">
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<?php 
				$course_subjects = array();
				if (have_rows('subject_area')):
					while (have_rows('subject_area')) : the_row();
					    $subject_array = array();
					        $subject_val = get_sub_field('subject');
					        array_push($subject_array,$subject_val);
					    $course_subjects = array_merge($course_subjects, $subject_array);
					endwhile;
				endif;
				
				$course_quarter = get_field('quarter');
				if ($course_quarter == 'f') {
					$quarter = 'Fall';
				}
				elseif ($course_quarter == 'w') {
					$quarter = 'Winter';
				}
				elseif ($course_quarter == 's') {
					$quarter = 'Spring';
				}
				elseif ($course_quarter == '1') {
					$quarter = 'Summer Session A';
				}
				elseif ($course_quarter == '2') {
					$quarter = 'Summer Session B';
				}
				
				$course_year = get_field('year');
				// Only grab the last two digits of year
				$year_digits = substr($course_year, -2);
				$course_program = get_field('program');
				// If user selects Both, then make variable empty
				if($course_program == 'B') {
					$course_program = '';
				}
				$term = $year_digits . $course_quarter;
				
				$exclude = array();
				if (have_rows('course_exclude')):
					while (have_rows('course_exclude')) : the_row();
					    $exclude_array = array();
					        $exclude_val = get_sub_field('exclude');
					        array_push($exclude_array,$exclude_val);
					    $exclude = array_merge($exclude, $exclude_array);
					endwhile;
				endif;
			?>
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
				<h1 class="page-title"><?php the_title(); ?></h1>
				<section>
					<?php the_content(); ?>
				</section>
			</article>
			<section>
				<h2><?php echo $quarter; ?> <?php echo $course_year; ?></h2>
				
				<?php include('library/saw/search_function.php');
				$subject_area_code = $course_subjects;
				$program = $course_program; //string U or G, empty string for both
				$quarters = array($term);
				$do_not_include = $exclude; //classes to remove from results (do not put leading zeros)
				$list = list_courses_by_subject_area_code($subject_area_code, $program, $quarters, $do_not_include); 
				
				foreach ($list as $subjectArea => $classes) { ?>
					
					<ul class="course-list">
				    
					<? // Class info
				    foreach ($classes as $class) { ?>
						<li>
					        <h3><?php echo $class["subjectAreaCode"]; ?> <?php echo $class["subjectClass"]; ?></h3>
							<strong>Instructor(s): </strong>
							<?php 
								if(empty($class["instructors"])) {
								    echo "No assigned instructor</br>";
								    continue;
								}
								foreach($class["instructors"] as $instructor) {
									$instructorName = $instructor['firstName'] . ' ' . $instructor['lastName'];
									$instructorName = ucwords(strtolower($instructorName));
									?><span class="instructor"><?php echo $instructorName; ?></span><?php 
								}
						    ?>
							<p>
								<? //if course description is empty, then show class description
								echo (empty($class["courseDescription"]) ? $class["classDescription"] : $class["courseDescription"]); ?>
							</p>
						</li>						
				    <?php } ?>
					
					</ul>
					
				<?php } ?>
			</section>
		</div>
		<?php get_sidebar(); ?>
	</div>
	<?php endwhile;  endif; ?>
<?php get_footer(); ?>