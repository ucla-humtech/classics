<?php get_header(); ?>
			<div id="main-content" role="main">
			<?php query_posts( 'posts_per_page=1' ); ?>
			<?php if ( have_posts() ) : ?>
				<?php while ( have_posts() ) : the_post(); 
				$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'home-hero' );
				$url = $thumb['0'];
				?>
				<a href="<?php the_permalink() ?>" class="hero-link">
					<div id="hero" class="desktop" style="background-image: url('<?php echo $url ?>');">
						<div class="content">
							<div class="hero-description">
								<div class="content">
									<h2><?php the_title(); ?></h2>
									<p><?php
									$content = get_the_content();
									$trimmed_content = wp_trim_words( $content, 32, '...' );
									echo $trimmed_content;
									?></p>
									<span class="btn outline">Read More</span>
								</div>
							</div>
						</div>
					</div>
					<div id="hero" class="mobile-hero" style="background-image:url('<?php echo $url ?>');">
					</div>
				</a>
				<?php endwhile; ?>
				<?php else : endif; ?>				
				<div class="content">
					<div class="col news-col three">
						<h3>Latest News</h3>
						<?php query_posts( array ( 'posts_per_page=4' ) ); ?>
						<ol>
							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
							<a href="<?php the_permalink() ?>">
								<li>
									<?php
										if ( has_post_thumbnail() ) {
											$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'article-thumb' );
											$url = $thumb['0']; ?>
											<img src="<?=$url?>" alt="<?php the_title(); ?>" />
										<?php } else { ?>
									        <img src="<?php echo get_template_directory_uri(); ?>/library/images/ucla-thumb.jpg" alt="A photo of <?php the_title(); ?>" />
									<?php } ?>
									<div class="item">
										<h4><?php the_title(); ?></h4>
										<p><?php
											$content = get_the_content();
											$trimmed_content = wp_trim_words( $content, 27, '...' );
											echo $trimmed_content;
										?></p>
									</div>
								</li>
							</a>							
							<?php endwhile; ?>
						</ol>
						<?php endif; ?>
						<a class="btn" href="/news/">View All</a>
					</div>
					<div class="col events-col one">
						<?php if ( is_active_sidebar( 'events-sidebar' ) ) : ?>
							<?php dynamic_sidebar( 'events-sidebar' ); ?>				
						<?php else : endif; ?>
					</div>
				</div>
			</div>
<?php get_footer(); ?>