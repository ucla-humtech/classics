<?php
/*
	Basic Navigation and Hero 
*/
?>

<a href="#main-content" class="hidden skip">Skip to main content</a>
<div id="container">
	<header role="banner" class="top">
		<div class="content">
			<a href="<?php echo home_url(); ?>">
				<img src="<?php echo get_template_directory_uri(); ?>/library/images/ucla-logo.svg" alt="UCLA" class="university-logo" />
			</a>
			<div class="dept-logo">             
			    <?php // Custom Logo code start
				if (has_custom_logo()) {  // Display the Custom Logo
					the_custom_logo();
				} else {  // No Custom Logo, just display the site's name ?>
				<a href="<?php echo home_url(); ?>"><h1 class="logo-text" alt="<?php the_field('department_name', 'option'); ?>"><?php bloginfo('name'); ?></h1></a>
				<?php } // Custom Logo code ends ?>
			</div>
			<?php if(get_field('enable_donation', 'option') == "enable") { ?>
			<div class="give-back">
				<?php if(get_field('link_type', 'option') == "internal") { ?>
				<a href="<?php the_field('donation_page', 'option'); ?>" class="btn give">
				<?php }?>
				<?php if(get_field('link_type', 'option') == "external") { ?>
				<a href="<?php the_field('donation_link', 'option'); ?>" class="btn give" target="_blank">
				<?php }?><span class="fas fa-heart" aria-hidden="true"></span>
				<?php the_field('button_text', 'option'); ?></a>
				<?php if(get_field('supporting_text', 'option')) { ?>
				<span class="support"><?php the_field('supporting_text', 'option'); ?></span>
				<?php }?>
			</div>
			<?php }?>
		</div>
		<nav role="navigation" aria-label="Main Navigation" class="desktop">
			<?php wp_nav_menu(array(
				'container' => false,
				'menu' => __( 'Main Menu', 'bonestheme' ),
				'menu_class' => 'main-nav',
				'theme_location' => 'main-nav',
				'before' => '',
				'after' => '',
				'depth' => 2,
			)); ?>
		</nav>
		<?php get_search_form(); ?>
	</header>
	<?php 
		// Don't do any of the below if homepage
		if ( is_front_page() ) { }
		// Breadcrumb everywhere else
		elseif ( is_single() || is_category() || is_archive() ) { 
			if ( function_exists('yoast_breadcrumb') ) {
				yoast_breadcrumb('<div class="breadcrumbs"><div class="content">','</div></div>');
			}
		}
		else {
			// Only show hero image on a page or post
			if ( has_post_thumbnail() && is_single() || has_post_thumbnail() && is_page() ) {
				$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'hero' );
				$url = $thumb['0']; ?>
	<div id="hero" style="background-image: url('<?=$url?>');">
		&nbsp;
	</div>
	<?php }
			// And show breadcrumb
			if ( function_exists('yoast_breadcrumb') ) {
				yoast_breadcrumb('<div class="breadcrumbs"><div class="content">','</div></div>');
			} 
		} 
	?>